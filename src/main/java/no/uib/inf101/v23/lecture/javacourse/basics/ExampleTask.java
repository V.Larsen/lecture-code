package no.uib.inf101.v23.lecture.javacourse.basics;

/**
 * Task: Create a new method that prints out the
 *  sentence "Jeg vil ha kake"
 */
public class ExampleTask {

    public static void main(String[] args) {
        printSentence();
    }

    public static void printSentence() {
        System.out.println("Jeg vil ha kake");
    }
    
}
