package no.uib.inf101.v23.lecture.javacourse.sequences;

import java.util.ArrayList;
import java.util.Arrays;

public class SequenceTypes {
    
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3));
        System.out.println(list);

        for (int i = 0; i < list.size(); i++) {
            int elem = list.get(i);
            System.out.println(elem);
        }

        ArrayList<ArrayList<Integer>> ls = new ArrayList<>();
        ArrayList<Integer> row1 = new ArrayList<>(Arrays.asList(1, 2, 3));
        ArrayList<Integer> row2 = new ArrayList<>(Arrays.asList(4, 5, 6));
        
        ls.add(row1);
        ls.add(row2);

        for (int i = 0; i < ls.size(); i++) {
            int elem = ls.get(1).get(1);
            System.out.println(elem);
        }

    }

}
