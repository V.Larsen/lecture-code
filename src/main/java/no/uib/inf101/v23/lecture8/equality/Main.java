package no.uib.inf101.v23.lecture8.equality;

public class Main {
  public static void main(String[] args) {
    Person p1 = new Person("Ola", "111111 12345");
    Person p2 = new Person("Ola", "111111 12345");
    System.out.println(p1.equals(p2));
  }
}

