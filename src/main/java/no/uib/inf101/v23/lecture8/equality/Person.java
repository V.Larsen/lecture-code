package no.uib.inf101.v23.lecture8.equality;

public class Person implements IPerson {
  String name;
  String id;

  public Person(String name, String id) {
    this.name = name;
    this.id = id;
  }

  @Override
  public void greet() {
    System.out.println("Hello, my name is " + this.name);
  }

}